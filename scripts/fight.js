let gasProgress = document.querySelector('.gas-progress');
let fightBtn = document.querySelector('.fight-btn');
let currentEnergy = document.querySelector('.current-energy');
let timeText = document.querySelector('.time');
let currentTime = 5;

let playerNameEle = document.querySelector('.player-name');
let urlParams = new URLSearchParams(window.location.search);
let playName = urlParams.get('player');
let gameId = urlParams.get('gameId');
playerNameEle.innerText = playName;

let fightReady = document.querySelector('.fight-ready');
let fightActive = document.querySelector('.fight-active');
let gasValue;

fightBtn.addEventListener('click', () => {
  if (currentTime > 0) {
    gasValue = gasProgress.value;
    gasProgress.value = gasValue + 1;
    gasValue = gasProgress.value;
    currentEnergy.innerText = gasValue;
  }
});

let countdownIntervalId = window.setInterval(function () {
  currentTime--;
  timeText.innerText = `${currentTime}s`;
}, 1000);

window.setTimeout(function () {
  window.clearTimeout(countdownIntervalId);
  gasValue = gasProgress.value;
  fightReady.classList.add('hide');
  fightActive.classList.remove('hide');
  console.log('fight', gasValue);

}, 5000);

let scoresAPI = 'https://655726a1.ngrok.io/api/scores';

window.setTimeout(function () {
  postData(scoresAPI, {
    name: playName,
    score: gasValue,
    game_id: gameId
  })
    .then(function (json) {
      console.log(json);
      location.href = `./result.html?gameId=${gameId}`;
    });
}, 12000);

function postData(url, data) {
  return fetch(url, {
    body: JSON.stringify(data),
    headers: {
      'Accept': 'application/json',
      'content-Type': 'application/json'
    },
    method: 'POST'
  })
    .then(response => response.json());
}