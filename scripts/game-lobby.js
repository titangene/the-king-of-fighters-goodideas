let playerList = document.querySelector('.player-list');
let currentPlayerCheck = document.querySelector('.current-player-check');

let urlParams = new URLSearchParams(window.location.search);
let playName = urlParams.get('player');
let gameId = urlParams.get('gameId');

// currentPlayerCheck.addEventListener('click', function () {
//   if (this.classList.contains('active')) {
//     this.innerText = 'Confirm';
//   } else {
//     this.innerText = 'Confirmed';
//   }
//   this.classList.toggle('active');
// });

// window.setInterval(function () {
//   window.location.href = `./fight.html?player=${playName}`;
// }, 2000);

const playListAPI = `https://655726a1.ngrok.io/api/playerlist?game_id=${gameId}`;

let jsonData;

fetch(playListAPI)
// fetch('./assets/data.json')
  .then(response => response.json())
  .then(function (json) {
    jsonData = json;
    console.log(json);

    playerList.innerHTML = '';
    json['players'].forEach(player => {
      playerList.innerHTML += `
        <div class="player">
          <input type="checkbox" name="player" id="${player.name}" class="player-check" checked>
          <span class="player-name">${player.name}</span>
        </div>`;
    });

    window.setTimeout(function () {
      location.href = `./fight.html?player=${playName}&gameId=${gameId}`;
    }, 16000);
  });

let timeText = document.querySelector('.time');
let currentTime = 16;

let countdownIntervalId = window.setInterval(function () {
  currentTime--;
  timeText.innerText = `${currentTime}s`;
}, 1000);

window.setInterval(function () {
  fetch(playListAPI)
    .then(response => response.json())
    .then(function (json) {
      jsonData = json;
      console.log(json);

      playerList.innerHTML = '';
      json['players'].forEach(player => {
        playerList.innerHTML += `
          <div class="player">
            <input type="checkbox" name="player" id="${player.name}" class="player-check" checked>
            <span class="player-name">${player.name}</span>
          </div>`;
      });
    });
}, 4000);