let gameFighterBtn = document.querySelector('.game-fighter');
let playNameElement = document.getElementById('player-name');

let playName;

const gameAPI = 'https://655726a1.ngrok.io/api/games';

gameFighterBtn.addEventListener('click', function() {
  playName = playNameElement.value;
  console.log(playName);

  if (playName) {
    postData(gameAPI, { name: playNameElement.value })
      .then(function (json) {
        console.log(json);
        let gameId = json.game_id;
        location.href = `./game-lobby.html?player=${playName}&gameId=${gameId}`;
      });
  } else {
    alert('請輸入玩家名稱');
  }
});

function postData(url, data) {
  return fetch(url, {
    body: JSON.stringify(data),
    headers: {
      'Accept': 'application/json',
      'content-Type': 'application/json'
    },
    method: 'POST'
  })
    .then(response => response.json());
}

function putData(url, data) {
  return fetch(url, {
    body: JSON.stringify(data),
    headers: {
      'Accept': 'application/json',
      'content-Type': 'application/json'
    },
    method: 'PUT'
  })
    .then(response => response.json());
}